package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegistrPage extends BasePage {

   @FindBy(xpath = "//*[@class=\"header__link header__link_register\"]")
    private WebElement openRegistration;

   @FindBy(xpath = "//*[@class=\"js-input-name\"]")
    private WebElement inputName;

   @FindBy(xpath = "//form/div/input[@type=\"email\"]")
    private WebElement inputEmail;

   @FindBy(xpath = "//form/div/input[@class=\"js-tel\"]")
    private WebElement inputNumber;

   @FindBy(xpath = "//*[@class=\"js-pass1\"]")
    private WebElement inputPassword;

   @FindBy(xpath = "//*[@class=\"js-pass2\"]")
    private WebElement inputRepeatPassword;

   @FindBy(xpath = "//*[@class=\"cookie__btn btn js-accept-cookies\"]")
    private WebElement cookie;

   @FindBy(xpath = "//button[@class=\"btn btn_arrow formPage__submit_large\"]")
    private WebElement clickRegistration;

   @FindBy(xpath = "//*[@class=\"popup__close btn btn_small js-close-popup\"]")
    private WebElement clickClose;

   @FindBy(css = ".input.formPage__input.formPage__input_full.input_error")
    private WebElement colorPassword;

   @FindBy(xpath = "//*[@class=\"popup popup_good popup_visible\"]")
   private WebElement registrationOk;



   public RegistrPage() {
       driver.get(config.baseUrl() + "/register");
       PageFactory.initElements(driver, this);
   }

   public void clickInputName(String textName){
       inputName.sendKeys(textName);
   }

   public void clickInputEmail(String textEmail){
       inputEmail.sendKeys(textEmail);
   }

   public void clickInputNumber(String textNumber){
       inputNumber.sendKeys(textNumber);
   }

   public void clickInputPassword(String textPassword){
       inputPassword.sendKeys(textPassword);
   }

   public void clickInputRepeatPassword(String textRepeatPassword){
       inputRepeatPassword.sendKeys(textRepeatPassword);
   }

   public void clickCookie(){
       cookie.click();
   }

   public void clickAgainRegistration(){
       clickRegistration.click();
   }

   public void clickTwoClose(){
       clickClose.click();
   }

   public String getColorPassword() {
      return colorPassword.getAttribute("class");
  }

  public String getRegistrationOk() {
       return registrationOk.getAttribute("class");
  }

}
