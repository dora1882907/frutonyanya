import org.junit.Test;
import pages.RegistrPage;

import static org.junit.Assert.assertEquals;

public class FrutoNyanyaTest extends BaseTest {

    @Test
    public void passwordIncorrect() {

        String name = "Женя";
        String email = "zhenya@mail.ru";
        String number = "9001002030";
        String password = "123456";
        String repeatPassword = "12345678";
        String expectedColorPassword = "input formPage__input formPage__input_full input_error";
        String resultsResponse = "Что-то пошло не так...";
        driver.get("https://frutonyanya.ru");


        RegistrPage registrPage = new RegistrPage();

        registrPage.clickInputName(name);
        registrPage.clickInputEmail(email);
        registrPage.clickInputNumber(number);
        registrPage.clickInputPassword(password);
        registrPage.clickInputRepeatPassword(repeatPassword);
        registrPage.clickCookie();
        registrPage.clickAgainRegistration();
        registrPage.clickTwoClose();

        assertEquals(expectedColorPassword, registrPage.getColorPassword());
    }

    @Test
     public void registration(){
        String name = "Женя";
        String email = "evgen@mail.ru";
        String number = "9001002033";
        String password = "123456789A";
        String repeatPassword = "123456789A";
        String expectedRegistration = "popup popup_good popup_visible";

        RegistrPage registrPage = new RegistrPage();

        registrPage.clickInputName(name);
        registrPage.clickInputEmail(email);
        registrPage.clickInputNumber(number);
        registrPage.clickInputPassword(password);
        registrPage.clickInputRepeatPassword(repeatPassword);
        registrPage.clickCookie();
        registrPage.clickAgainRegistration();

        assertEquals(expectedRegistration, registrPage.getRegistrationOk());
    }
}





      //  WebElement openRegistration = driver.findElement(By.xpath("//*[@class=\"header__link header__link_register\"]"));
       // openRegistration.click();

        //WebElement inputName = driver.findElement(By.xpath("//*[@class=\"js-input-name\"]"));
       // WebElement inputName = driver.findElement(By.cssSelector(".js-input-name"));
 //       inputName.sendKeys(name);

   //     WebElement inputEmail = driver.findElement(By.xpath("//form/div/input[@type=\"email\"]"));
        //WebElement inputEmail = driver.findElement(By.cssSelector(".input.formPage__input"));
     //   inputEmail.sendKeys(email);

       // WebElement inputNumber = driver.findElement(By.xpath("//form/div/input[@class=\"js-tel\"]"));
        //inputNumber.sendKeys(number);

 //       WebElement inputPassword = driver.findElement(By.xpath("//*[@class=\"js-pass1\"]"));
 //       inputPassword.sendKeys(password);

 //       WebElement inputRepeatPassword = driver.findElement(By.xpath("//*[@class=\"js-pass2\"]"));
  //      inputRepeatPassword.sendKeys(repeatPassword);
//
 //       WebElement cookie = driver.findElement(By.xpath("//*[@class=\"cookie__btn btn js-accept-cookies\"]"));
//        cookie.click();

//        WebElement clickRegistration = driver.findElement(By.xpath("//button[@class=\"btn btn_arrow formPage__submit_large\"]"));
  //      clickRegistration.click();


       // WebElement badResponse = driver.findElement(By.xpath("//*[text()=\"Что-то пошло не так...\"]"));

       // assertEquals(resultsResponse, badResponse.getText());

 //        WebElement clickClose = driver.findElement(By.xpath("//*[@class=\"popup__close btn btn_small js-close-popup\"]"));
 //       clickClose.click();

//        WebElement colorPassword = driver.findElement(By.cssSelector(".input.formPage__input.formPage__input_full.input_error"));

  //      assertEquals(expectedColorPassword, colorPassword.getAttribute("class"));//если был найден этот элемент, значит тест пройден
  //  }
